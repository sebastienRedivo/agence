<?php

namespace App\Notification;

use App\Entity\Contact;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class ContactNotification
{

    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function notify(Contact $contact){
        $message = (new TemplatedEmail())
                ->From('devseb34@gmail.com')
                ->to('devseb34@gmail.com')
                ->subject('contact pour : '.$contact->getProperty()->getTitle())
                ->replyTo($contact->getEmail())
                ->htmlTemplate('emails/contact.html.twig')
                ->context(['contact' => $contact])
                ;
        $this->mailer->send($message);
    }

}