<?php
namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\PropertyType;
use App\Form\UserUpdateType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminUserController extends AbstractController{

    /***
     * @var PropetyRpository
     */
    private $repository;

    /***
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(UserRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }


    /**
     * @Route("/admin/users", name="admin.users")
     */
    public function listUsers(Request $request)
    {
        $user = $this->repository->findAll();

        return $this->render('admin/user/list.html.twig', [
                'users' => $user
        ]);
    }

    /**
     * @Route("/admin/user/edit/{id}", name="admin.user.edit")
     */
    public function userEdit(Request $request, User $user)
    {
        $form = $this->createForm(UserUpdateType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUpdatedAt(new \DateTime());
            $this->em->flush();
            $this->addFlash('success', 'modifier avec succès');

            return $this->redirectToRoute('admin.users');
        }
        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/user/{id}/delete", name="admin.user.delete", methods="DELETE")
     */
    public function delete(User $user, Request $request): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->get('_token'))) {
            $this->em->remove($user);
            $this->em->flush();
            $this->addFlash('success', 'supprimmer avec succès');
        }
        return $this->redirectToRoute('admin.users');

    }
}