<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\ContactType;
use App\Form\PropertySearchType;
use App\Notification\ContactNotification;
use App\Repository\PropertyRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController
{
    private $repository;


    public function __construct(PropertyRepository $repository)
    {
        $this->repository = $repository;
  
    }


    /**
     * @Route("/properties", name="properties")
     * @return Response 
     */
    public function index(Request $request): Response
    {
        /**
         * declaration de l'entitiée search pour le filtre de recherche
         */
        $search = new PropertySearch();
        /** formulaire de filtre */
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);
        /** FIn filtre de recherche */
        /**envoi de la recherche dans le Query */
        
        /*$propertiesQuery = $this->repository->findAllVisible($search);
        /*$properties = $paginatorInterface->paginate($propertiesQuery,
                                                    $request->query->getInt('page', 1), 15 );
                                                    */
                                                   
        return $this->render('properties/index.html.twig',
                                ['current_menu'=>'active',
                                'properties' => $this->repository->paginateAllVisible($search, $request->query->getInt('page', 1) ),
                                'form'=> $form->createView()]);
    }

    /**
     * @Route("/property/{slug}/{id}", name="property.show")
     */
    public function show(Property $property, String $slug, Request $request, ContactNotification $contactNotification): Response
    {
       
         if ($property->getSlug() !== $slug) {
            return $this->redirectToRoute('property.show',[
                'id'=> $property->getId(),
                'slug' => $property->getSlug()
            ], 301);
        }

        $contact = new Contact();
        $contact->setProperty($property);
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $contactNotification->notify($contact);
            $this->addFlash('success', 'Message bien envoyer');
            return $this->redirectToRoute('property.show',[
                'id'=> $property->getId(),
                'slug' => $property->getSlug()
            ]);
        }

        return $this->render('properties/show.html.twig',
        ['current_menu'=>'active',
         'property'=> $property,
         'form' => $form->createView()
        ]);
    }

}