
        let map = document.querySelector('#map')
        
        if(map === null){
           
        }
        let center =[map.dataset.lat, map.dataset.lng]
        map = L.map('map').setView(center, 13)
        let token = 'pk.eyJ1Ijoic2ViZmlyZSIsImEiOiJjamplM2swMzcxN2RtM2tvbG0xMzV2eWd0In0.lSi0Kb9h-mkFzP6ERT0UnQ'
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: token
        }).addTo(map)
        L.marker(center).addTo(map)    
    