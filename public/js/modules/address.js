let inputAddress = document.querySelector('#property_address')
if (inputAddress !== null) {
    var placesAutocomplete = places({
        appId: "plVQ15AMF3BF",
        apiKey: "e2edca5092c499d68cd8e637a463fd03",
        container: document.querySelector("#property_address"),
    });
    placesAutocomplete.on("change", (e) => {
        document.querySelector("#property_city").value =
            e.suggestion.city;
        document.querySelector("#property_postalCode").value =
            e.suggestion.postcode;
        document.querySelector("#property_lat").value =
            e.suggestion.latlng.lat;
        document.querySelector("#property_lng").value =
            e.suggestion.latlng.lng;
    });
}

let searchAddress = document.querySelector('#search_address')
if (searchAddress !== null) {
    var placesAutocomplete = places({
        appId: "plVQ15AMF3BF",
        apiKey: "e2edca5092c499d68cd8e637a463fd03",
        container: document.querySelector("#search_address"),
    });
    placesAutocomplete.on("change", (e) => {
        document.querySelector("#lat").value =
            e.suggestion.latlng.lat;
        document.querySelector("#lng").value =
            e.suggestion.latlng.lng;
    });
}